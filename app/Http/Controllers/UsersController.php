<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;
use Hash;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

       public function register()
    {
    	return view('user.register');
    }

    public function create(Request $request){
        $input = $request->input();

        $user = new User;

        $user->firstname = $input['firstname'];
        $user->lastname = $input['lastname'];
        $user->username = $input['username'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['lastname']);


        $user->save();
        return view('user.login');
        // $data = $user['attributes'];
        // $id = $data['id'];
    
    // return Redirect::action('UsersController@show', array('user' => $user));
    }


    public function show($id){

        $user = User::findOrFail($id);
         return view('pages.homepage', compact('user'));
    }


}
