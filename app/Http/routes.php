<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});

Route::get('login', 'SessionsController@create');


Route::get('logout', 'SessionsController@logout');

Route::resource('sessions', 'SessionsController', [ 'only' => ['index', 'create', 'store']]);

Route::resource('tasks', 'TasksController', [ 'only' => ['index', 'create', 'show', 'store']]);

Route::resource('lists', 'ListsController', [ 'only' => ['create', 'show', 'store']]);

Route::get('homepage', 'PagesController@homepage', ['middleware' => 'auth']);

Route::post('create', 'UsersController@create');

Route::get('register', 'UsersController@register');

Route::get('user/{id}', 'UsersController@show');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
