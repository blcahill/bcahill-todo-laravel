<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskList extends Model
{

	public $table = "tasklists";

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function task()
    {
    	return $this->hasMany('App\Task');
    }
}
