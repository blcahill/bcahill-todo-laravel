@extends('master')

@section('content')
	<div class="actions">
		<div class="content-wrapper">
			<h1>Create A New List</h1>
			
			<form>
				<h4>Name Your List!</h4>
				<input type="text" name="title">
				<input class="submit-button" type="submit" value="Create List">
			</form>
		</div>
	</div>
@stop