<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<title> Too Many To Do Lists</title>
	<link rel="stylesheet" href="{{asset('css/welcome.css')}}">
	<link rel="stylesheet" href="{{asset('css/homepage.css')}}">
	<link rel="stylesheet" href="{{asset('css/login.css')}}">
</head>

<header>
	<div class="content-wrapper">
		<a href="{{ URL::to('/') }}" class="home-link">Too Many To Do Lists</a>
	</div>
</header>
<body>
	<div class="page-container">
		<div class="content-wrapper">
		@yield('content')

		</div>
	</div>
</body>
<footer>
	<!-- <a href="https://bitbucket.org/blcahill/bcahill-todo-laravel">On Bitbucket</a> -->
</footer>
</html>