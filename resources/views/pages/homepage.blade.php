@extends('master')

@section('content')

	<div class="left">
		<div class="user-details">
			<h3>My Info</h3>
			<p class="name">{{ $firstname }} {{ $lastname }}</p>
			<p class="email"> {{ $email }}</p>
			
			<a href="{{ action('SessionsController@logout') }}" class="logout">Log Out</a>
			
		</div>

		<div class="lists">
			<h3>My Lists</h3>
			<ul>
				<li>
					<a href="#"><!-- {{$user->tasklists()}} --></a>
				</li>
			</ul>
		</div>

		<div class="task-list-actions">
			<div class="new-task">
				<a href="{{ action('TasksController@create') }}">New Task</a>
			</div>

			<div class="new-list">
				<a href="{{ action('ListsController@create') }}">New List</a>
			</div>
		</div>

	</div>
	<div class="right">
		<div class="open-tasks">
			<h3>Things to Do</h3>
			<ul>
				<li>
					<p></p>
				</li>
			</ul>
		</div>

		
		<div class="in-progress-tasks">
			<h3>Tasks in the works</h3>
			<ul>
				<li>
					<p></p>
				</li>
			</ul>
		</div>

		
		<div class="complete-tasks">
			<h3>Done!!</h3>
			<ul>
				<li>
					<p></p>
				</li>
			</ul>
		</div>

	</div>
@stop