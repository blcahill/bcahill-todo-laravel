@extends('master')

@section('content')
    <div class="landing-action">
        <h1>Welcome!</h1>
        <div class="actions">
        	<div class="content-wrapper">
            <a href="/login">Log In</a>
            
            <a href="/register">Register</a>
            </div>
        </div>
    </div>
@stop
