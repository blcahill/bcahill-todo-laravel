@extends('master')

@section('content')
	<div class="actions">
		<div class="content-wrapper">
			<h1>Create A New Task</h1>
			
			<form>
				<h4>Name Your Task!</h4>
				<input type="text" name="description">
				<input class="submit-button" type="submit" value="Create Task">
			</form>
		</div>
	</div>

@stop