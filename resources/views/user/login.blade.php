@extends('master')

@section('content')
	<div class="actions">
		<div class="content-wrapper">
		<h1>Log In</h1>
		
		<form method="POST" action="{{ action('SessionsController@store') }}">
			<h4>Email</h4>
			<input type="text" name="email">
			<h4>Password</h4>
			<input type="text" name="password">
			<input class="submit-button" type="submit" value="Log In">
		</form>
	</div>
	</div>
@stop