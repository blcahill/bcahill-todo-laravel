@extends('master')

@section('content')
	<div class="actions">
		<div class="content-wrapper">
			<h1>Register An Account</h1>
			
			<form method="POST" action="{{ action('UsersController@create') }}" >
				<h4>First Name</h4>
				<input type="text" name="firstname">
				<h4>Last Name</h4>
				<input type="text" name="lastname">
				<h4>Username</h4>
				<input type="text" name="username">
				<h4>Email</h4>
				<input type="text" name="email">
				<h4>Password</h4>
				<input type="text" name="password">
				<input class="submit-button" type="submit" value="Sign Up">
			</form>
		</div>
	</div>
@stop